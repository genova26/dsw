<?php
    include "utils/utils.php"; 
    $nombre = $email = $asunto = $apellido = $mensaje = "";

    // ----------------------------------------------------------------------------------------
    // ------------------------------ Array de Errores ----------------------------------------
    // ----------------------------------------------------------------------------------------
    $errores = array(   
        0 => "*El nombre es requerido.",
        1 => "*No se ha indicado email o el formato no es correcto.",
        2 => "*El asunto es requerido, mínimo 5 caracteres.."
    );

    // ----------------------------------------------------------------------------------------
    // ----------------------------------- Filtrado -------------------------------------------
    // ----------------------------------------------------------------------------------------
    function filtrado($datos){
        $datos = trim($datos); // Elimina espacios antes y después de los datos
        $datos = stripslashes($datos); // Elimina backslashes \
        $datos = htmlspecialchars($datos); // Traduce caracteres especiales en entidades HTML
        return $datos;
    }

    if(isset($_POST["enviar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){
        $nombre = filtrado($_POST["nombre"]);
        $apellido = filtrado($_POST["apellido"]);
        $email = filtrado($_POST["email"]);
        $asunto = filtrado($_POST["asunto"]);
        $mensaje = filtrado($_POST["mensaje"]);
    }

    // ----------------------------------------------------------------------------------------
    // ----------------------------------- Nombre ---------------------------------------------
    // ----------------------------------------------------------------------------------------
    function nombreErr($errores, $nombre){
        if(isset($_POST["enviar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

            if(isset($_POST["nombre"]) && empty( $nombre ) || strlen( $nombre ) < 1){
                return $errores[0];

            }else if(empty($errores)){
                return "";
            }
        }
    }

    // ----------------------------------------------------------------------------------------
    // ----------------------------------- Email ----------------------------------------------
    // ----------------------------------------------------------------------------------------
    function emailErr($errores, $email){
        if(isset($_POST["enviar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

            if(!filter_var($_POST["email"], FILTER_VALIDATE_EMAIL) || empty( $email )){
                return $errores[1];

            }else if(empty($errores)){
                return "";
            }
        }
    }

    // ----------------------------------------------------------------------------------------
    // ----------------------------------- Asunto ---------------------------------------------
    // ----------------------------------------------------------------------------------------
    function asuntoErr($errores, $asunto){
        if(isset($_POST["enviar"]) && $_SERVER["REQUEST_METHOD"] == "POST"){

            if(isset($_POST["asunto"]) && empty( $asunto ) || strlen( $asunto ) < 5){
                return $errores[2];

            }else if(empty($errores)){
                return "";
            }
        }
    }

    require "views/contact.view.php";
?>