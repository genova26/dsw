<?php
    function esActiva($op_menu) : string{

        // strpos — Encuentra la posición de
        // la primera ocurrencia de un substring en un string

        // Donde Buscar
        $uri = $_SERVER["REQUEST_URI"];
        // Busqueda
        $find = $op_menu;
        // Resultado
        $pos = strpos($uri, $find);

        if ($pos === false) {
            
            return false;
        } else {

            return true;
        }
    }

?>
