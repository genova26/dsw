<!----------------------------------------------------------------
-------------------- PHP - Controlador  --------------------------
---------------------------------------------------------------->
<?php 
    // Si la variable salir esta definida.
    if ( isset($_POST['salir'] ) ) {
        // Redirigir el navegador a game.php.
        header("Location: index.php");
        return;
    }

    // Sal.
    $salt = 'XyZzy12*_';
    // Clave cifrada md5.
    $stored_hash = '1a52e17fa899cf40fb04cfc42e6352f1';  // Contra php123.
    $vacio = false;  // Si no tenemos datos POST.

    // Comprueba si tenemos datos POST.
    // Si la variable nombre y contraseña esta definida.
    if ( isset($_POST['nombre']) && isset($_POST['contra']) ) {

        // Si la contraseña o el nombre es menor a uno
        if ( strlen($_POST['nombre']) < 1 || strlen($_POST['contra']) < 1 ) {
            // Muestra el siguiente texto.
            $vacio = "Se requiere nombre de usuario y clave para acceder.";
        } 
        else {
            // Concatenamos $salt con la contraseña dada y lo pasamos a md5.
            $md5 = hash('md5', $salt.$_POST['contra']);
            // Si $md5 es igual a $stored_hash.
            if ( $md5 == $stored_hash ) {
                // Redirigir el navegador a game.php.
                header("Location: game.php?nombre=".urlencode($_POST['nombre']));
                return;
            } else {
                // Muestra el siguiente texto.
                $vacio = "Contraseña Incorrecta.";
            }
        }
    }
?>

<!----------------------------------------------------------------
--------------------- HTML - Vista -------------------------------
---------------------------------------------------------------->
<!DOCTYPE html>
<html lang="en">
    <head>
        
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Login</title>
    </head>
<body>
    <div class="container">
        <h1>Inicie sesión.</h1>
        <?php
            // Si vacio no es falso.
            if ( $vacio !== false ) {
                // Muestra el contenido del mismo en rojo.
                echo('<p style="color: red;">'.htmlentities($vacio)."</p>\n");
            }
        ?>

        <form method="POST">
            
            <label for="nombre">Nombre</label>
            <input type="text" name="nombre" id="nombre"><br/>

            <label for="contra">Contraseña</label>
            <input type="text" name="contra" id="contra"><br/>
            
            <!-- -- Botones -- -->
            <!-- Enviar -->
            <input type="submit" value="Enviar">
            <!-- Salir -->
            <input type="submit" name="salir" value="Salir">
        </form>
    </div>
</body>
