<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Inicio</title>
</head>
<body>
    <h1>Bienvenido a Piedra, Papel y Tijera</h1>
    <br>
    <a href="login.php">Por favor, inicie sesión.</a>
    <br>
    <p>
        Intenta ir a <a href="game.php">game.php</a> sin iniciar sesión - 
        debería fallar con un mensaje de error.
    </p>
</body>
</html>