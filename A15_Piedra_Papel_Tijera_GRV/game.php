<!----------------------------------------------------------------
-------------------- PHP - Controlador  --------------------------
---------------------------------------------------------------->

<?php

    // Si no esta definida la variable nombre o mide menos de 1
    if ( ! isset($_GET['nombre']) || strlen($_GET['nombre']) < 1  ) {
        // Muestra el siguiente texto.
        die('Falta el parámetro nombre.');
    }

     // Si esta definida la variable salir
    if ( isset($_POST['salir']) ) {
        // Redirigir el navegador a index.php.
        header('Location: index.php');
        return;
    }

    // Array con los objetos
    $objetos = array('Piedra', 'Papel', 'Tijeras');
    // Si la variable no esta vacia
    $usuario = isset($_POST["usuario"]) ? $_POST['usuario']+0 : -1;
    // Contrincante
    $valor = 0; // Piedra

    function check($valor, $usuario) {

        // Si el usuario saca Piedra
        if ( $usuario == 0 ) 
        {
            return "Empate";
        } 
        // Si el usuario saca Papel
        else if ( $usuario == 1 ) 
        {
            return "Ganaste";
        } 
        // Si el usuario saca Tijeras
        else if ( $usuario == 2 ) 
        {
            return "Perdiste";
        }

        return false;
    }

    // El resultado es igual a la funcion 
    $result = check($valor, $usuario);

?>

<!----------------------------------------------------------------
--------------------- HTML - Vista -------------------------------
---------------------------------------------------------------->
<!DOCTYPE html>
<html>
    <head>
        <title>Juego</title>
    </head>
<body>
    <div>
        <h1>Piedra, Papel y Tijeras</h1>
            <?php
                // $_REQUEST es un array asociativo que por defecto 
                // contiene el contenido de $_GET, $_POST y $_COOKIE.

                //  Si la request nombre esta definida.
                if ( isset($_REQUEST['nombre']) ) {
                    // Muestra el siguiente texto.
                    echo "<p>Bienvenido: ".htmlentities($_REQUEST['nombre'])."</p>\n";
                }
            ?>

            <form method="post">
                <!-- Menu desplegable -->
                <select name="usuario">
                    <option value="-1">Selecciona</option>
                    <option value="0">Piedra</option>
                    <option value="1">Papel</option>
                    <option value="2">Tijeras</option>
                    <option value="3">Test</option>
                </select>

                <!-- -- Botones -- -->
                <!-- Jugar -->
                <input type="submit" value="Jugar">
                <!-- Salir -->
                <input type="submit" name="salir" value="Salir">
            </form>

            <pre>
                <?php
                    // Si el usuario selecciona el "-1"
                    if ( $usuario == -1 ) {
                        // Muestra el siguiente texto.
                        print "Selecciona tu arma y dale a Jugar.\n";
                    
                    // Si el usuario selecciona el "3"
                    } else if ( $usuario == 3 ) {
                        
                        // Contrincante
                        for( $c = 0; $c < 3; $c++ ) {

                            // Usuario
                            for( $h = 0; $h < 3; $h++) {

                                // Resultado
                                $r = check($c, $h);
                                // Muestra el siguiente texto.
                                print "Jugaste $objetos[$h], tu contrincante jugo $objetos[$c] y $r. \n";
                            }
                        }
                    } else {
                        // Muestra el siguiente texto.
                        print "Has jugado $objetos[$usuario], tu contrincante ha jugado $objetos[$valor] y $result. \n";
                    }
                ?>
            </pre>
    </div>
</body>
</html>