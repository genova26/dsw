<?php

    # A11. Funciones.

    # -----------------------------------------------------------
    # Ejercicio 1.

    # 1. Explica la utilidad de la siguiente sentencia PHP. 
    # Comprueba su funcionalidad con un ejemplo:
    # declare(strict_types=1);

    # Funcionalidad 
    # Esta sentencia es un constructor denominado "declare",
    # se usa para fijar directivas de ejecución para un 
    # bloque de código, es decir, al declarar "strict_types"
    # en nuestros ficheros forzará a que nuestros métodos acepten 
    # variables únicamente del tipo exacto que se declaren. 
    # En caso contrario lanzará un TypeError.

    # Esto hará que nuestro desarrollo sea más robusto, y que 
    # nuestros métodos no se traguen cualquier cosa. 
    # Esto se debe declarar en la primera línea, nada más abrir PHP
    # y en cada archivo si así lo deseamos.

    # Ejemplo Correcto
    declare(strict_types=1);
    echo "<title>GenovaRV</title>";
    echo "<h3>Ejercicio 1</h3>";
    echo "Hola, me llamo Genova.";

    # Ejemplo Incorrecto
    # const TICK_VALUE = 1;
    # declare(ticks=TICK_VALUE);


    # 1.1 En PHP es posible especificar el tipo de dato que 
    # devuelve una función. 
    # Investiga cómo y prueba un ejemplo.

    echo "<h3>Ejercicio 1.1</h3>";
    function cuadrado($núm){
        return $núm * $núm;
    }
    echo "El resultado de la función es: ".cuadrado(4).".";
    

    # 1.2 Cómo especificaríamos que una función puede
    # devolver un String o el valor null? 

    # Podemos especificar que una función devuelva un string 
    # poniendo ": string" como se ve en el siguiente ejemplo.
    # En el caso de que no pongamos nada la función podrá devolver
    # un valor null.

    echo "<h3>Ejercicio 1.2</h3>";

    # La siguiente función devolverá un string.
    function greeting2() : string{
        return "Hola";
    }
    print greeting2() . " Genova.\n";


    # -----------------------------------------------------------
    # Ejercicio 2.

    # 2. Queremos crear una función llamada insert que devuelva 
    # una sentencia "insert into" de sql en formato string.

    echo "<h3>Ejercicio 2</h3>";

    # Función por valor
    function insert ($alumnos, $datos): string {
            $keys = array_keys($datos);
            # print_r($keys);
            $separado_por_puntos = implode(" :", $keys);
            $separado_por_comas = implode(", ", $keys);
            $cadena = "insert into $alumnos ($separado_por_comas) values (:$separado_por_puntos)";
        return $cadena;
    }

    # echo "<br>";
    echo insert('alumnos', ["id"=>1, "nombre"=>'Jesús', "curso"=>'2º DAW']);



    # -----------------------------------------------------------
    # Ejercicio 3. Paso de parámetros por referencia.

    echo "<h3>Ejercicio 3. Paso de parámetros por referencia</h3>";

    # Función por referencia
    function insert2 ($alumnos, $datos, &$cadena) {
        $keys = array_keys($datos);
        # print_r($keys);
        $separado_por_puntos = implode(" :", $keys);
        $separado_por_comas = implode(", ", $keys);
        $cadena = "insert into $alumnos ($separado_por_comas) values (:$separado_por_puntos)";
    }

    # echo "insert por referencia <br>";
    insert2('alumnos', ["id"=>1, "nombre"=>'Jesús', "curso"=>'2º DAW'], $cadena);
    echo  $cadena;


    # ------------ Otro Ejermplo ------------
    echo "<h3>Otro Ejemplo</h3>";
    function foo(&$var)
    {
        $var++;
    }
    
    // Definimos la variable
    $a=5;
    // La usamos en la función
    foo($a);
    // Imprimimos
    echo "Resultado: $a."; // $a es 6 aquí
    

    








?>