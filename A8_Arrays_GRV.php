<?php

    # A8 Arrays
    echo "<title>GenovaRV</title>";
    
    # -------------------------------------------------------------------
    # 1. Crea un array llamado nombres que contenga varios nombres.

    $nombres = array(
            "Genova", 
            "Javi", 
            "Maria", 
            "Alex",
            "Juan",
            "Sara",
            "Beatriz",
            "Paula",
            "Raul",
            "Daniel"
    );

    # -------------------------------------------------------------------
    # 2. Muestra el número de elementos que tiene el array (función count).

    echo "<br>";
    echo "El array contiene ".count($nombres)." elementos.";
    echo "<br>";

    # -------------------------------------------------------------------
    # 3. Crea una cadena que contenga los nombres de los alumnos existentes 
    # en el array separados por un espacio y muéstrala (función de strings 
    # implode).

    echo "<br>";
    $separado_por_espacios = implode(" ", $nombres);
    echo "El array contiene los siguientes nombres: "
    .$separado_por_espacios."."; 
    echo "<br>";

    # -------------------------------------------------------------------
    # 4. Muestra el array ordenado alfabéticamente (función asort).

    echo "<br>";
    # Ordena por valor
    asort($nombres);
    echo "Nombres ordenados alfabeticamente: "; 
    foreach ($nombres as $key => $val) {
        echo "$val\n";
    }
    echo "<br>";

    # -------------------------------------------------------------------
    # 5. Muestra por pantalla las diferencias con ksort y con sort.

    echo "<br>";
    # Ordena por clave
    echo "KSORT:  "; 
    ksort($nombres);
    foreach ($nombres as $key => $val) {
        echo "$val\n";
    }
    echo "<br>";

    echo "<br>";
    # Ordena un array
    echo "SORT:  "; 
    sort($nombres);
    foreach ($nombres as $clave => $valor) {
        echo "$valor\n";
    }
    echo "<br>";

    # -------------------------------------------------------------------
    # 6. Muestra el array en el orden inverso al que se creó 
    # (función array_reverse).

    echo "<br>";
    echo "Nombres al reves: "; 
    $reversed = array_reverse($nombres);
    print_r($reversed);
    echo "<br>";

    # -------------------------------------------------------------------
    # 7. Muestra la posición que tiene tu nombre en el array 
    # (función array_search).

    echo "<br>";
    $clave = array_search('Genova', $nombres);
    print_r("La posición del nombre es ".$clave.".");
    echo "<br>";

    # -------------------------------------------------------------------
    # 8. Crea un array de alumnos donde cada elemento sea otro array que 
    # contenga el id, nombre y edad del alumno.

    $alumnos = array(

        0 => array(
            "id" => '1',
            "nombre" => 'Genova',
            "edad" => '21'
        ),

        1 => array(
            "id" => '2',
            "nombre" => 'Juan',
            "edad" => '22'
        ),

        2 => array(
            "id" => '3',
            "nombre" => 'Pedro',
            "edad" => '24'
        ),

        3 => array(
            "id" => '4',
            "nombre" => 'Manuela',
            "edad" => '19'
        )
        
    );

    # -------------------------------------------------------------------
    # 9. Crea una tabla html en la que se muestren todos los datos de 
    # los alumnos.

    echo "<br>";
    echo "Tabla Alumnos";
    ?>
    <table border="1">
    <?php
    foreach ( $alumnos as $r ) {
            ?>
                <tr>
                    <?php
                        foreach ( $r as $v ) {
                    ?>
                        <td><?php echo $v;?></td>
                    <?php
                        }
                    ?>
                </tr>
            <?php
        }
    ?>
    </table>
    <?php
    echo "<br>";

    # -------------------------------------------------------------------
    # 10. Utiliza la función array_column para obtener un array indexado  
    # que contenga únicamente los nombres de los alumnos y muéstralo por 
    # pantalla.

    echo "Nombres del array alumnos: ";
    $nombres = array_column($alumnos, 'nombre');
    print_r($nombres);
    echo "<br>";

    # -------------------------------------------------------------------
    # 11. Crea un array con 10 números y utiliza la función array_sum para  
    # obtener la suma de los 10 números.
    
    echo "<br>";
    $num = array(
        1,
        2,
        3,
        4,
        5,
        6,
        7,
        8,
        9,
        10
    );

    echo "La suma del array da " . array_sum($num) . ".\n";

?>