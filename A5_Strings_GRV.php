<?PHP
    echo "<title>GenovaRV</title>";

    // Ejercicio 1

    echo "<br>";
    echo "Ejercicio 1";
    echo "<br>";

    // 1. Muestra el valor de un parámetro llamado nombre recibido por querystring.

    // echo '¡Hola ' . htmlspecialchars($_GET["usuario"].'!');  // Opcion
    // echo "<br>";

    // $nombre_usuario = $_GET['usuario'];
    // echo $nombre_usuario;
    // echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // 1.1 Si no se pasa un parámetro nombre se utilizará tu nombre de pila. Utiliza 
    // el operador de fusión a null de PHP para comprobar si se pasa o no el parámetro por la URL.

    // Obntener el valor de $_GET['usuario'] y devolver 'Genova' (Nombre de Pila)
    // si no existe.

    // $nombre_usuario = $_GET['usuario'] ?? 'Genova';
    // echo $nombre_usuario;
    // echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // 1.2 Elimina los caracteres '/' del principio y el final del parámetro de consulta por $_GET si los hubiera -
    // (función trim).

    $nombre_usuario = $_GET['usuario'] ?? 'Genova';
    $str = $nombre_usuario;
    print trim($str, $character_mask = "/");
    echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // Ejercicio 2

    echo "<br>";
    echo "Ejercicio 2";
    echo "<br>";

    // 2. Muestra la longitud del parámetro nombre (función strlen).

    print ("Logitud del parámetro nombre: ");
    print strlen($nombre_usuario);
    echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // Ejercicio 3

    echo "<br>";
    echo "Ejercicio 3";
    echo "<br>";

    // 3. Muestra el nombre en mayúsculas (función strtoupper) y en minúsculas (función strtolower).

    // Mayúsculas
    print ("Función Strtoupper: ");
    print strtoupper($nombre_usuario.".");
    echo "<br>";

    // Minúsculas
    print ("Función Strtolower: ");
    print strtolower($nombre_usuario.".");
    echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // Ejercicio 4

    echo "<br>";
    echo "Ejercicio 4";
    echo "<br>";

    // 4. Pasa un segundo parámetro por querystring llamado prefijo (para pasar más de un parámetro por la 
    // querystring debes separarlos utilizando el carácter &). Después indica si el nombre comienza
    // por el prefijo pasado o no (función strpos), se distinguirá entre mayúsculas y minúsculas.
    // Si no se pasa el prefijo no se realizará esta operación.

    $prefijo = $_GET['prefijo'];
    $posicion = stripos($nombre_usuario, $prefijo);
    if( $posicion !== false){
        print($posicion);
    }
    echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // Ejercicio 5

    echo "<br>";
    echo "Ejercicio 5";
    echo "<br>";

    // 5. Muestra el número de veces que aparece la letra 'a' (mayúscula o minúscula) en el parámetro nombre 
    // (funciones substr_count + (strtoupper o strtolower)).

    $cadena2 = "a";
    print ("La 'a' aparece ".substr_count(strtoupper($nombre_usuario), strtoupper($cadena2))." vez.");
    echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // Ejercicio 6

    echo "<br>";
    echo "Ejercicio 6";
    echo "<br>";

    // 6. Muestra la posición de la primera 'a' existente en el nombre (sea mayúscula o minúscula). Si no hay 
    // ninguna mostrará un texto indicándolo (función stripos).

    $cadena2 = "a";
    print ("La  posición de la primera 'a' es ".stripos($nombre_usuario, $cadena2).".");
    echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // Ejercicio 7

    echo "<br>";
    echo "Ejercicio 7";
    echo "<br>";

    // 7. Muestra el nombre sustituyendo las letras ‘o’ por el número cero (sea mayúscula o minúscula) 
    // (función str_ireplace).

    print ("Sustitución de la 'o' por '0': ".str_ireplace( "o", "0",$nombre_usuario ).".");
    echo "<br>";

    // ------------------------------------------------------------------------------------------------------

    // Ejercicio 8

    echo "<br>";
    echo "Ejercicio 8";
    echo "<br>";

    // 8. La función parse_url nos permite extraer distintas partes de una url. A partir de una variable que 
    // contenga una url, por ejemplo:
    $url = 'http://username:password@hostname:9090/path?arg=value';

    // Utiliza la función parse_url para extraer y mostrar por pantalla las siguientes partes de la 
    // variable url del ejemplo:
    // - El protocolo utilizado (en el ejemplo http).

    print ("El protocolo utilizado en la url es '".parse_url($url, PHP_URL_SCHEME)."'.");
    echo "<br>";

    // - El nombre de usuario (en el ejemplo username).

    print ("El nombre de usuario utilizado en la url es '".parse_url($url, PHP_URL_USER)."'.");
    echo "<br>";

    // - El path de la url (en el ejemplo /path).

    print ("El path utilizado en la url es '".parse_url($url, PHP_URL_PATH)."'.");
    echo "<br>";

    // - El querystring de la url (en el ejemplo arg=value).

    print ("El querystring utilizado en la url es '".parse_url($url, PHP_URL_QUERY)."'.");
    echo "<br>";


?>