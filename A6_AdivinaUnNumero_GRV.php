<?php
    # A6. Adivina un numero.
    # --------------------------------------------------------------
    # Tu nombre debe estar en la etiqueta <title> de la página.
    echo "<title>GenovaRV</title>";

    # --------------------------------------------------------------
    echo "<h1>A6. Adivina el Número.</h1>";
    echo "<br>";

    # Si no se introduce un dato en la variable esta sera 0.
    $num = htmlspecialchars($_GET["numero"] ?? "0");
    $valor = 6;

    # $valor = rand(1, 7); // Numero random.
    
    # --------------------------------------------------------------
    # Si en la URL se introduce un valor no numérico el programa 
    # responderá "Debes introducir un número" (función is_numeric).

    if ( is_numeric( $num ) ) {
        # Si en la URL no hay ningún parámetro el programa responderá 
        # "No has especificado ningún número".
        if( $num == false ){
            print("No has introducido ningún número.");
        } 
        
        # Si en la URL se introduce un número mayor que el que ha 
        # pensado el programa, el script responderá "Mi número es 
        # menor".
        else if( $num > $valor ){
            print("Mi número es menor.");
        }

        # Si en la URL se introduce un número menor que el que ha 
        # pensado el programa, el script responderá "Mi número es mayor".
        else if( $num < $valor ){
            print("Mi número es mayor.");
        }

        # Si en la URL se introduce el número que ha pensado el programa, 
        # el script responderá "Enhorabuena. Has acertado".
        else if( $num == $valor){
            print("Enhorabuena. Has acertado.");
        }

    } else {
        # Es que has introducido un caracter o letra.
        echo "Debes introducir un número";
    }

?>