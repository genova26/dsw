<!-- A9. MD5 Cracker --> 
<!DOCTYPE html>
<head><title>GenovaRV</title></head>
<body>
<h1>MD5 cracker</h1>
<p>This application takes an MD5 hash
of a two-character lower case string and 
attempts to hash all two-character combinations
to determine the original two characters.</p>
<pre>
Debug Output:
<?php
$goodtext = "Not found";
if ( isset($_GET['md5']) ) {
    $time_pre = microtime(true);
    $md5 = $_GET['md5'];

    $txt = "abcdefghijklmnopqrstuvwxyz";
    $show = 15;

    for($i=0; $i<strlen($txt); $i++ ) {
        $ch1 = $txt[$i];   

        for($j=0; $j<strlen($txt); $j++ ) {
            $ch2 = $txt[$j];  

            $try = $ch1.$ch2;

            $check = hash('md5', $try);
            if ( $check == $md5 ) {
                $goodtext = $try;
                break;   
            }

            if ( $show > 0 ) {
                print "$check $try\n";
                $show = $show - 1;
            }
        }
    }

    $time_post = microtime(true);
    print "Elapsed time: ";
    print $time_post-$time_pre;
    print "\n";
}
?>
</pre>

<p><?= htmlentities($goodtext); ?></p>
<form>
<input type="text" name="md5" size="60" />
<input type="submit" value="Crack MD5"/>
</form>

</body>
</html>

<?php
$md5 = "Not computed";
if ( isset($_GET['encode']) ) {
    $md5 = hash('md5', $_GET['encode']);
}
?>


https://github.com/csev/wa4e/tree/master/code/crack

https://www.wa4e.com/solutions/crack/index.php?md5=0bd65e799153554726820ca639514029

